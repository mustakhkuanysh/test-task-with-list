import React from "react";
import { useDispatch } from "react-redux";
import { addItem, removeItem } from "./redux/listSlice";
import List from "./components/List";

const App = () => {
  const dispatch = useDispatch();

  const handleAddItem = () => {
    dispatch(addItem());
  };

  const handleRemoveItem = () => {
    dispatch(removeItem());
  };

  return (
    <div>
      <div
        style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
      >
        <button onClick={handleAddItem}>Добавить</button>
        <button onClick={handleRemoveItem}>Удалить</button>
      </div>
      <List />
    </div>
  );
};

export default App;
