import { createSlice } from "@reduxjs/toolkit";

const listSlice = createSlice({
  name: "list",
  initialState: [],
  reducers: {
    addItem: (state, action) => {
      const newColor = generateRandomColor();
      state.unshift({ color: newColor });
    },
    removeItem: (state) => {
      state.pop();
    },
  },
});

const generateRandomColor = () => {
  const letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export const { addItem, removeItem } = listSlice.actions;

export default listSlice.reducer;
