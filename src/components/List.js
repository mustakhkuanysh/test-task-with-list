import React from "react";
import { useSelector } from "react-redux";
import { useTransition, animated } from "react-spring";

const List = () => {
  const list = useSelector((state) => state.list);

  const transitions = useTransition(list, {
    from: { opacity: 0, transform: "translateX(-100%)" },
    enter: { opacity: 1, transform: "translateX(0%)" },
    leave: { opacity: 0, transform: "translateX(100%)" },
    unique: true,
    key: (item) => item.id,
  });

  return (
    <div style={{ display: "flex" }}>
      {transitions((style, item) => (
        <animated.div
          style={{
            ...style,
            backgroundColor: item.color,
            width: "20%",
            height: "20vw",
          }}
        />
      ))}
    </div>
  );
};

export default List;
